// Justin Henn
// cs4410 hw1
// 09/21/2017

#include <gl/glut.h>   
#include <stdlib.h>    
#include <time.h>  

class GLintPoint {

public:
	GLint x, y;
};

//initialization

void myInit(void) {

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 400.0, 0.0, 400.0);
}

//create diamonds

void diamond(GLintPoint center, int size) {

	float r1 = rand() / (float)RAND_MAX;
	float g1 = rand() / (float)RAND_MAX;
	float b1 = rand() / (float)RAND_MAX;

	glColor3f(r1, g1, b1);

	glBegin(GL_POLYGON);
	glVertex2i(center.x, (center.y + size));
	glVertex2i((center.x - size), center.y);
	glVertex2i(center.x, (center.y - size));
	glVertex2i((center.x + size), center.y);
	glEnd();
	glFlush();
}

//create information for diamonds and call create diamond

void createDiamond() {

	glClear(GL_COLOR_BUFFER_BIT);

	GLintPoint Dia1 = { 35, 55 };
	GLintPoint Dia2 = { 310, 365 };
	GLintPoint Dia3 = { 75, 20 };
	GLintPoint Dia4 = { 220, 80 };

	diamond(Dia1, 35);
	diamond(Dia2, 80);
	diamond(Dia3, 20);
	diamond(Dia4, 60);
}

//main

void main(int argc, char** argv) {

	srand(time(NULL));
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(400, 400);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("Diamond");
	glutDisplayFunc(createDiamond);
	myInit();
	glutMainLoop();
}