// Justin Henn
// cs4410 hw1
// 09/21/2017

#include <gl/glut.h> 
#include <stdlib.h>    
#include <time.h>

//initialization     

void myInit(void) {

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 250.0, 0.0, 250.0);
}

//create checkerboard

void checkerboard(int size) {

	glClear(GL_COLOR_BUFFER_BIT);

	float r1 = rand() / (float)RAND_MAX;
	float r2 = rand() / (float)RAND_MAX;
	float g1 = rand() / (float)RAND_MAX;
	float g2 = rand() / (float)RAND_MAX;
	float b1 = rand() / (float)RAND_MAX;
	float b2 = rand() / (float)RAND_MAX;

	for (int x = 0; x < 8; x++) {

		for (int y = 0; y < 8; y++) {

			if ((x + y) % 2 == 0)
				glColor3f(r1, g1, b1);
			else
				glColor3f(r2, g2, b2);
			glRecti(x*size, y*size, (x + 1)*size, (y + 1)*size);
		}
	}
	glFlush();
}

//call to create checkerboard

void createBoard() {

	checkerboard(30);
}

//main

void main(int argc, char** argv) {

	srand(time(NULL));
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(250, 250);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("CheckerBoard");
	glutDisplayFunc(createBoard);
	myInit();
	glutMainLoop();
}