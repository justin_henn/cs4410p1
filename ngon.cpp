// Justin Henn
// cs4410 hw1
// 09/21/2017

#include <gl/glut.h>   
#include <stdlib.h>    
#include <time.h>
#include <math.h>

class GLintPoint {

public:
	GLint x, y;
};

double pi = 3.141592;

//initialization

void myInit(void) {

	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0.0, 400.0, 0.0, 400.0);
}

//create polygon

void createPoly(GLintPoint center, int ngon) {

	float r1 = rand() / (float)RAND_MAX;
	float g1 = rand() / (float)RAND_MAX;
	float b1 = rand() / (float)RAND_MAX;
	glClear(GL_COLOR_BUFFER_BIT);

	glColor3f(r1, g1, b1);
	int size = (rand() % 70 + 20);

	glBegin(GL_POLYGON);

	for (int i = 0; i < ngon; i++) {

		float total = (2 * pi * i) / ngon;
		glVertex2f((size * cos(total) + center.x), (size * sin(total) + center.y));
	}
	glEnd();
	glFlush();
}

//initialize polygon random numbers and call createPoly

void poly() {

	int ngon = (rand() % 7) + 3;
	GLintPoint P1 = { rand() % 400, rand() % 400 };
	createPoly(P1, ngon);
}

//keyboard function

void key(unsigned char ch, int x, int y) {

	if (isalpha(ch)) {
		poly();
	}
}

//main

void main(int argc, char** argv) {

	srand(time(NULL));
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize(400, 400);
	glutInitWindowPosition(100, 150);
	glutCreateWindow("Polygons");
	glutDisplayFunc(myInit);
	glutKeyboardFunc(key);
	myInit();
	glutMainLoop();
}