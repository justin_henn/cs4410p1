// Justin Henn
// cs4410 hw1
// 09/21/2017

#include <gl/glut.h>   
#include <vector>

std::vector<int> v;
int j = 0, r = 0;
int z = 0;

//initialization

void myInit(void) {

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, 400, 0, 400);
	glMatrixMode(GL_MODELVIEW);
	glClearColor(1.0, 1.0, 1.0, 1.0);
	glViewport(0, 0, 400, 400);

}

//check to see if mouse is pressed down

void mouse(int button, int state, int x, int y) {

	if (state == GLUT_DOWN) {

		if (button == GLUT_LEFT_BUTTON) {

			j = 1;
			v.push_back(x);
			v.push_back(y);
			glutPostRedisplay();

		}


		else if (button == GLUT_RIGHT_BUTTON) {

			j = 0;
			glClear(GL_COLOR_BUFFER_BIT);
			v.clear();
			glFlush();
		}
	}
}


//check for motion

void motion(int x, int y)
{

	if (j == 1) {
		v.push_back(x);
		v.push_back(y);
		glutPostRedisplay();
	}
}

//display contents

void display() {

	glClear(GL_COLOR_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_LINE_STRIP);

	for (int i = 0; i < v.size(); i += 2)
	{
		glVertex2i(v[i], (400 - v[i + 1]));
	}
	glEnd();

	glutSwapBuffers();

}

//main

void main(int argc, char** argv) {

	glutInit(&argc, argv);
	glutInitWindowSize(400, 400);
	glutInitWindowPosition(100, 150);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutCreateWindow("Diamond");



	glutMouseFunc(mouse);
	glutMotionFunc(motion);

	glutDisplayFunc(display);
	myInit();
	glutMainLoop();


}